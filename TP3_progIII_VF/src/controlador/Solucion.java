package controlador;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import modelo.ArbitrosEquipos;
import modelo.Calendario;
import modelo.Fecha;
import modelo.Partido;

public class Solucion {

	public static ArrayList<Fecha> ejecutar(Calendario c) {

		// Agrega arbitros a una lista segun la cantidad de fechas (tiene 3)
//                Integer ultimoArbitro = 0;
		if(c != null)
		{
			ArrayList<Integer> arbitros = new ArrayList<Integer>();
			for (int a = 0; a < c.getFecha().getPartidos().size(); a++) {
				arbitros.add(0);
			}
	                
	
			ArrayList<Fecha> ret = c.getCalendario();
			Integer _arbitro = 0;
	
			for (Fecha f : ret) {
	                        HashSet<Integer> arbitrosEnPartido = new HashSet<Integer>();
				for (Partido p : f.getPartidos()) {
	                                ArbitrosEquipos.agregarTupla(p.getEquipoLocal().getNombre(), f.getPartidos().size());
	                                ArbitrosEquipos.agregarTupla(p.getEquipoVisitante().getNombre(), f.getPartidos().size());
					if (p.getArbitro() == 0)
						_arbitro = mejorArbitro(f.getPartidos().size(), p, f.getUltimoArbitro(), arbitros, arbitrosEnPartido);
					p.setArbitro(_arbitro);
	                                f.setUltimoArbitro(_arbitro);
	                                arbitrosEnPartido.add(_arbitro);
	//				ultimoArbitro = _arbitro;
	                                //System.out.println(_arbitro);
				}
	                        
	             //           System.out.println("\n\n");
			}
			return ret;
		}else {
			throw new IllegalArgumentException("El calendario es nulo");
		}
	}

	private static Integer mejorArbitro(Integer arbitro, Partido p, Integer ultimoArbitro,
			ArrayList<Integer> arbitros, HashSet<Integer> arbitrosEnPartido) {

		double promedio = 100.0;
		Integer mArbitro = null;
		int cont = 1;
		HashMap<Integer, Integer> arbitroEquipoLocal = ArbitrosEquipos.getAparicionesArbitros(p.getEquipoLocal().getNombre());
		HashMap<Integer, Integer> arbitroEquipoVisitante = ArbitrosEquipos.getAparicionesArbitros(p.getEquipoVisitante().getNombre());

		while (cont <= arbitro) {
			if (cont != ultimoArbitro) {
                            if(!arbitrosEnPartido.contains(cont)){
				if (promedio > ((double) arbitroEquipoLocal.get(cont) + (double) arbitroEquipoVisitante.get(cont))
						/ 2) {
					if (arbitroEquipoLocal.get(cont) < p.getEquipoLocal().getPeso()
							&& arbitroEquipoVisitante.get(cont) < p.getEquipoVisitante().getPeso()) {
							arbitros.set(cont - 1, arbitros.get(cont - 1) + 1);
							promedio = ((double) arbitroEquipoLocal.get(cont)
									+ (double) arbitroEquipoVisitante.get(cont)) / 2;
							mArbitro = cont;
					}
				}
                            }
			}
			cont++;
		}
		ArbitrosEquipos.sumarArbitros(p.getEquipoLocal().getNombre(), mArbitro);
		ArbitrosEquipos.sumarArbitros(p.getEquipoVisitante().getNombre(), mArbitro);
		return mArbitro;
	}

}

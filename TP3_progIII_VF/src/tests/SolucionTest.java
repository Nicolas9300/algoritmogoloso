package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import controlador.Solucion;
import modelo.ArchivoJson;
import modelo.Calendario;

public class SolucionTest {

	
	  @Test public void arbitroAdecuadosTest(){
	  
	  Calendario c = crearCalendario(); Solucion.ejecutar(c);
	  assertFalse(Asserts.ahiArbitrosExecididos(c));
	  
	  }
	 
	
	@Test
	public void promedioArbitros(){
		
		Calendario c = crearCalendario();
		Solucion.ejecutar(c);
		System.out.println(Asserts.promedioArbitroAdecuado(c));
		assertEquals(0, Double.compare(2.17, Asserts.promedioArbitroAdecuado(c)));
		
	}

	private Calendario crearCalendario() {
		
            Calendario c = ArchivoJson.leerJSON("calendarioNuevo.json");
		
            return c;
	}

	
	@Test(expected = IllegalArgumentException.class)
	public void calendarioNuloTest() {
		
		Calendario c = null;
		
		Solucion.ejecutar(c);
		
	}
	
}

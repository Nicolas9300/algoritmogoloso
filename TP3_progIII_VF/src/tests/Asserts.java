package tests;

import java.util.ArrayList;
import java.util.HashMap;
import modelo.ArbitrosEquipos;

import modelo.Calendario;
import modelo.Fecha;
import modelo.Partido;
import modelo.Tupla;

public class Asserts {
	public static boolean ahiArbitrosExecididos(Calendario c) {

		ArrayList<Fecha> ret = c.getCalendario();
		boolean capacidadExcedida = false;
		for(Fecha f : ret) {
			
			for (Partido p: f.getPartidos()) {
				HashMap<Integer, Integer> arbitroEquipoLocal = ArbitrosEquipos.getAparicionesArbitros(p.getEquipoLocal().getNombre());
                                HashMap<Integer, Integer> arbitroEquipoVisitante = ArbitrosEquipos.getAparicionesArbitros(p.getEquipoVisitante().getNombre());
			   for(Integer a = 1; a <= f.getPartidos().size(); a++) {
					capacidadExcedida = capacidadExcedida || (arbitroEquipoLocal.get(a) > 3 || arbitroEquipoVisitante.get(a) > 3);
				
			   }
			}
		}
		
		return capacidadExcedida;
	}

	public static double promedioArbitroAdecuado(Calendario c) {
		  
	     double promedio = 0.0;
	     
	 
	  for(Tupla t : ArbitrosEquipos.getArbitros()){
	   int mayorAparicion = 0;
	   for(int i = 1 ; i <= t.getAparicionesArbitro().size(); i++) {
	    
	    if(mayorAparicion < t.getAparicionesArbitro().get(i)) {
	     mayorAparicion = t.getAparicionesArbitro().get(i);
	    
	   }
	   }
	   promedio += mayorAparicion;
	 }
	  promedio = promedio / ((c.getFecha().getPartidos().size())*2);
	  double resultado = Math.round(promedio*100.0)/100.0;
	  
	  return resultado;

	}
}


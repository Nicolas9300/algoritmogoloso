package modelo;

import java.util.ArrayList;

public class CalendarioData {
	private Calendario calendario;

    public CalendarioData(Calendario calendario) {
        this.calendario = calendario;
    }
    
    public ArrayList<String> getEquiposPorFecha(int fecha)
    {
        ArrayList<String> ret = new ArrayList<String>();
        
        Fecha f = calendario.getCalendario().get(fecha);
        
        for(Partido p : f.getPartidos())
        {
            ret.add(p.getEquipoLocal().getNombre());
            ret.add(p.getEquipoVisitante().getNombre());
        }
        
            
        return ret;
    }
    
    public ArrayList<Integer> getPartidosPorFecha(int fecha)
    {
        ArrayList<Integer> ret = new ArrayList<Integer>();
        
        Fecha f = calendario.getCalendario().get(fecha);
        
        for(Partido p : f.getPartidos())
        {
            ret.add(p.getArbitro());
        }
        
        return ret;
    }
    
}

package modelo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ArchivoJson {

	private ArrayList<Calendario> calendarios;

	public ArchivoJson() {

		calendarios = new ArrayList<Calendario>();
	}

	public void addPartido(Calendario c) {

		calendarios.add(c);

	}

	public int tamanio() {
		return calendarios.size();
	}

	public static String generarJSON(Calendario c) {

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(c);

		return json;

	}

	public static Calendario leerJSON(String archivo) {

		String json = "";

		try {

			BufferedReader br = new BufferedReader(new FileReader(archivo));

			String linea;
			while ((linea = br.readLine()) != null) {
				json += linea;
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

		Gson gson = new Gson();
		Calendario c = gson.fromJson(json, Calendario.class);

		return c;

	}

	public static void guardarJSON(String jsonParaGuardar, String archivoDestino) {

		try {
			FileWriter writer = new FileWriter(archivoDestino);
			writer.write(jsonParaGuardar);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
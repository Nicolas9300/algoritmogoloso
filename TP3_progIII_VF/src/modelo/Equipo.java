package modelo;

import java.util.HashMap;

public class Equipo implements Comparable<Equipo>{
	private String nombre;
	private Integer capacidad;
	
	public Equipo(String n) {
		nombre = n;
		capacidad = 4; // El limite de arbitros que puede tener el equipo
		
		
		/*Agregue la capacidad pero creo que quizas iria en otro lugar*/
		
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public Integer getPeso() {
		return capacidad;
	}

	@Override
	public int compareTo(Equipo o) {
		return capacidad - o.getPeso();
	}
	
	
}

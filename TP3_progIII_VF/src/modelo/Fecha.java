package modelo;

import java.util.ArrayList;

public class Fecha {
	ArrayList<Partido> partidos;
	Integer ultimoArbitro = 0;
	
	public Fecha() {
		partidos = new ArrayList<Partido>();
	}
	
	
	public void agregarPartido(Partido p) {
		if(partidos.contains(p)) {
			throw new RuntimeException("El partido ya existe!");
		}
		partidos.add(p);
	}
	
	
	public Integer getUltimoArbitro() {
		return ultimoArbitro;
	}


	public void setUltimoArbitro(Integer ultimoArbitro) {
		this.ultimoArbitro = ultimoArbitro;
	}


	public ArrayList<Partido> getPartidos() {
		return partidos;
	}


	@Override
	public String toString() {
		return "Fecha [partidos=" + partidos + ", ultimoArbitro=" + ultimoArbitro + "]";
	}
	
	
	
}

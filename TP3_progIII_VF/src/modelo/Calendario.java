package modelo;

import java.util.ArrayList;

public class Calendario {

	private ArrayList<Fecha> calendario;

	public Calendario() {
		calendario = new ArrayList<Fecha>();
	}

	public Integer cantidadFechas() {
		return calendario.size();
	}

	public ArrayList<Fecha> getCalendario() {
		return calendario;
	}

	public void setListaDeFechas(ArrayList<Fecha> c) {
		calendario = c;
	}

	public void agregarFecha(Fecha f) {

		calendario.add(f);

	}

	public Fecha getFecha() {
		return calendario.get(0);
	}

	@Override
	public String toString() {
		return "Calendario [calendario=" + calendario + "]";
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author bravomartin
 */
public class ArbitrosEquipos {
    
    private static ArrayList<Tupla> arbitros = new ArrayList<Tupla>();
    
    
    public static void agregarTupla(String equipo, Integer size)
    {
        boolean aparece = false;
        if(arbitros.isEmpty()){
            arbitros.add(new Tupla(equipo, size));
        }else{
        for(Tupla t : arbitros)
        {
            if(equipo.equals(t.getEquipo()))
            {
                aparece = true;
            }
        }
        if(!aparece)
            arbitros.add(new Tupla(equipo, size));
        }
    }
    
    public static HashMap<Integer, Integer> getAparicionesArbitros(String equipo)
    {
            for(Tupla t : arbitros)
        {
            if(equipo.equals(t.getEquipo()))
            {
                return t.getAparicionesArbitro();
            }
        }
            
          return null;
    }
    
    public static void sumarArbitros(String equipo, Integer arbitro)
    {
         for(Tupla t : arbitros)
        {
            if(equipo.equals(t.getEquipo()))
            {
                t.setAparicionesArbitro(arbitro);
            }
        }
    }

	public static ArrayList<Tupla> getArbitros() {
		return arbitros;
	}
    
    
    
}

package modelo;

public class Partido {

	private Equipo equipo1;
	private Equipo equipo2;
	private Integer arbitro = 0;

	public Partido(Equipo e1, Equipo e2) {
		equipo1 = e1;
		equipo2 = e2;
	}

	public Equipo getEquipoLocal() {
		return equipo1;
	}

	public Equipo getEquipoVisitante() {
		return equipo2;
	}

	public Integer getArbitro() {
		return arbitro;
	}

	public void setArbitro(Integer arbitro) {
		this.arbitro = arbitro;
	}

	@Override
	public String toString() {
		return "Partido [equipo1=" + equipo1 + ", equipo2=" + equipo2 + ", arbitro=" + arbitro + "]";
	}

}

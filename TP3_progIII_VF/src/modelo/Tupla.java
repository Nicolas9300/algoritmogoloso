/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.HashMap;

/**
 *
 * @author bravomartin
 */
public class Tupla {
    
    private String equipo;
    private HashMap<Integer, Integer> aparicionesArbitro;
    private int contador = 1;

    public Tupla(String equipo, Integer size) {
        this.equipo = equipo;
        this.aparicionesArbitro = new HashMap<Integer, Integer>();
        

        while(contador <= size){
            this.aparicionesArbitro.put(contador, 0);
            contador++;
        }
    }

    public void setAparicionesArbitro(Integer arbitro) {
        aparicionesArbitro.put(arbitro, aparicionesArbitro.get(arbitro)+1);
    }

    public HashMap<Integer, Integer> getAparicionesArbitro() {
        return aparicionesArbitro;
    }

    public String getEquipo() {
        return equipo;
    }
    
    
}

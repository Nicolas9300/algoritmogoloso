package vista;

import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.event.ActionListener;

import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.UIManager;

import modelo.ArchivoJson;
import modelo.Calendario;
import controlador.Solucion;

import java.awt.Color;

public class Menu implements ActionListener {

	private JFrame frame;
	private JPanel panel;
	private JButton btnMostrarFechas;
	private JButton btnEmpezar;
	private Calendario calendario;
        private String archivoJsonEnUso;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frame.setVisible(true);
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Menu() {
		initialize();
	}

	private void initialize() {
                archivoJsonEnUso = "calendarioNuevo.json";
		calendario = new Calendario();
		frame = new JFrame();
		frame.setBounds(100, 100, 601, 493);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setTitle("Algoritmos Golosos: Grupo Coffee");
		panel = new JPanel();
		panel.setBounds(0, 0, 595, 464);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		btnEmpezar = new JButton("Empezar!");
		btnEmpezar.setForeground(Color.BLACK);
		btnEmpezar.setBackground(Color.LIGHT_GRAY);
		btnEmpezar.addActionListener((ActionListener) this);
		btnEmpezar.setBounds(401, 390, 168, 34);
		btnEmpezar.setFocusable(false);
		panel.add(btnEmpezar);

		btnMostrarFechas = new JButton("Ver calendario");
		btnMostrarFechas.setForeground(Color.BLACK);
		btnMostrarFechas.setBackground(Color.LIGHT_GRAY);
		btnMostrarFechas.addActionListener((ActionListener) this);
		btnMostrarFechas.setBounds(230, 390, 134, 34);
		btnMostrarFechas.setFocusable(false);
		panel.add(btnMostrarFechas);

		JLabel lblImg = new JLabel("");
		lblImg.setBounds(0, 0, 594, 465);
		ImageIcon imagen = new ImageIcon("src/imagenes/programming.jpg");
		Icon icono = new ImageIcon(
				imagen.getImage().getScaledInstance(lblImg.getWidth(), lblImg.getHeight(), Image.SCALE_DEFAULT));
		lblImg.setIcon(icono);
		panel.add(lblImg);
	}

	// Eventos de los botones.

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnMostrarFechas) {
			Fixture fixture = new Fixture();
			fixture.obtenerJson(archivoJsonEnUso);
			fixture.visible(true);

		}

		if (e.getSource() == btnEmpezar) {
			calendario = ArchivoJson.leerJSON(archivoJsonEnUso);
			calendario.setListaDeFechas(Solucion.ejecutar(calendario));
			
			String resultado = ArchivoJson.generarJSON(calendario);
			ArchivoJson.guardarJSON(resultado, "calendario.json");
                        archivoJsonEnUso = "calendario.json";
		}

	}

}

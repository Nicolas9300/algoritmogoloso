package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

import modelo.ArchivoJson;
import modelo.Calendario;
import modelo.CalendarioData;


import javax.swing.border.LineBorder;
import java.awt.Color;

import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;

public class Fixture {

	private JFrame frame;
	private JPanel panel;
	
	private ArrayList<String> equipos;
	private ArrayList<Integer> arbitros;
	private ArrayList<JTable> tablas;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Fixture window = new Fixture();
					window.frame.setVisible(true);
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Fixture() {
		initialize();
	}

	private void initialize() {
		equipos = new ArrayList<String>();
		arbitros = new ArrayList<Integer>();
		frame = new JFrame();
		frame.setBounds(100, 100, 820, 493);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setTitle("Calendario");

		panel = new JPanel();
		panel.setBounds(0, 0, 804, 454);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		
		crearTablas();
		
		JLabel lblFecha1 = new JLabel("Fecha 1");
		lblFecha1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblFecha1.setBounds(96, 21, 75, 14);
		panel.add(lblFecha1);

		JLabel lblFecha = new JLabel("Fecha 2");
		lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblFecha.setBounds(364, 21, 75, 14);
		panel.add(lblFecha);

		JLabel lblFecha_1 = new JLabel("Fecha 3");
		lblFecha_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblFecha_1.setBounds(632, 21, 75, 14);
		panel.add(lblFecha_1);

		JLabel lblFecha_2 = new JLabel("Fecha 5");
		lblFecha_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblFecha_2.setBounds(364, 225, 75, 14);
		panel.add(lblFecha_2);

		JLabel lblFecha_3 = new JLabel("Fecha 4");
		lblFecha_3.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblFecha_3.setBounds(96, 225, 75, 14);
		panel.add(lblFecha_3);
		
		
		
	}
	
	public void crearTablas() {
		tablas = new ArrayList<JTable>();
		int x = 20;
		int y = 46;
		for(int i = 0; i < 5; i++) {
			JTable tabla = new JTable();
			tabla.setToolTipText("");
			tabla.setBorder(new LineBorder(new Color(0, 0, 0)));
			tabla.setBounds(x, y, 228, 135);
			tabla.setModel(new DefaultTableModel(
					new Object[][] { {null, null, null}, { null, null, null }, { null, null, null }, { null, null, null }, },
					new Object[] {"", "", "" }));
			tabla.getColumnModel().getColumn(0).setResizable(false);
			tabla.getColumnModel().getColumn(1).setResizable(false);
			tabla.getColumnModel().getColumn(2).setResizable(false);
			tabla.setRowHeight(34);
			tablas.add(tabla);
			panel.add(tabla);
			x = x + 268;
			if( i == 2) {
				x = 20;
				y = y + 204;
			}
			
		}
	}
	
	public void visible(boolean b) {
		frame.setVisible(b);
	}

	public void obtenerJson(String archivo) {
		
		Calendario calendario = ArchivoJson.leerJSON(archivo);
		mostrar(calendario);
	}
	
	

	private void mostrar(Calendario calendario) {
		
		CalendarioData CD = new CalendarioData(calendario);

		for (int k = 0; k < calendario.cantidadFechas(); k++) {
			equipos = CD.getEquiposPorFecha(k);
			arbitros = CD.getPartidosPorFecha(k);
			String matriz[][] = new String[4][3];
			matriz[0][0] = "Local";
			matriz[0][1] = "Visitante";
			matriz[0][2] = "Arbitro";
			int c = 0;
			int f = 1;
			for (int i = 0; i < equipos.size(); i++) {
				matriz[f][c] = equipos.get(i);
				c += 1;
				if (c > 1) {
					c = 0;
					f += 1;
				}
			}
			f = 1;
			for(int i = 0; i < arbitros.size(); i++)
            {
                matriz[f][2] = String.valueOf(arbitros.get(i));
                f++;
            }
			
			cargarDatos(k, matriz);

		}
	}
	
	
	

	/**
	 * Metodo que se encarga de cargar las tablas con los partidos y sus respectivos
	 * equipos.
	 */
	
	
	
	private void cargarDatos(int index, String[][] matriz) {

		tablas.get(index).setModel(new javax.swing.table.DefaultTableModel(matriz, new String[] { "", "", "" }));
	}

}
